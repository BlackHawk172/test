#!/bin/bash
INPUT=users.csv
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read username givenname surname password email
do
echo "$username $givenname $surname $password $email"
samba-tool user add "$username" "$password"  --given-name="$givenname" --surname="$surname" --mail-address="$email"
done < $INPUT
IFS=OLDIFS
